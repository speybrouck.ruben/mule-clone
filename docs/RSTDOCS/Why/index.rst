.. _why:

Why?
~~~~~~


Simon Sinek, bestselling author of the book "start with why" and speaker at the third most popular ted talk of all time, researched dozens of the most successful companies around the globe to find out what
separates the great from the average. He concluded that great organisations do not focus on product nor the means to the end,
but on "why?". Knowing why you do something leads to a fulfilling life and a gratifying career and a great organisation with focus and intrinsically motivated people.

.. figure:: /images/why/sinek.jpg
    :width: 400px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Simon Sinek in action.

.. note:: His exact mantra is "People do not buy what you do; but why you do it". Find Simon Sinek's full Ted talk here: https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action?referrer=playlist-the_most_popular_talks_of_all


.. toctree::
   :maxdepth: 2

   ideology/index
   Similar-success/index