.. M.U.L.E. Documentation documentation master file, created by Ruben Marc Speybrouck
   sphinx-quickstart on Tue Jul  3 11:51:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

M.U.L.E. business plan
==========================

Welcome to the M.U.L.E. business plan.
This business plan exists to convince you to (co-)create a platform for following and autonomous robots. Think robotic mules.

You are among a handful of close friends and family, who have been given a copy because I need your feedback and hope for your support and possible contribution.
This document's purpose is to find  people who want to take this journey with me.

Let one thing be clear, if you received a copy it is because I hope for your contribution and honest feedback and hopefully, an interest to join me.
The goal at this stage is NOT to raise capital.

.. note:: This is the very first draft. Nothing in here is set in stone nor a definitive plan. I am absolutely certain there are grammar and spelling mistakes all over this document, but that is not the point. The plan refers to "We" throughout most of it but at this stage, consists of only me. Also please do not share this document, it was meant for you.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   RSTDOCS/What/index
   RSTDOCS/Why/index
   RSTDOCS/How/index
   RSTDOCS/Who/index
   RSTDOCS/Where/index
   RSTDOCS/When/index
   RSTDOCS/Other/index


