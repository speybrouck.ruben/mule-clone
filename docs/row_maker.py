contents = [
    [
        ".. image:: /images/cloudApp.png\n   :width: 200px",
        ":ref: `cloud_index`",
        "Get started with Pozyx's Cloud offering today!"
    ],
    [
        ".. image:: /images/cloudApp.png\n   :width: 200px",
        ":ref: `diy_index`",
        "Get started with Pozyx's DIY offering today!"
    ]
]


def max_line_length(contents):
    for content in contents:
        pass